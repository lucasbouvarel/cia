exports.order = function order(req, res, next) {
  // TODO implement from here
  console.log(req.body)

  if(!req.body.quantities){
    res.status(400).send()
    return
  }
  if(!req.body.country){
    res.status(400).send()
    return
  }
  if(!req.body.prices){
    res.status(400).send()
    return
  }
  if(!req.body.reduction){
    res.status(400).send()
    return
  }

  var quantities = req.body.quantities
  var country = req.body.country
  var reduction = req.body.reduction
  var prices= req.body.prices
  var taxe=0
  var totalHT=0
  var totalTTC=0
  var totalFinal=0

  if(quantities.error){
    res.status(400).send()
  }
  if(prices.error){
    res.status(400).send()
  }
  if(prices.length == quantities.length){
    for (var i = 0; i < prices.length; i++) {

      totalHT= totalHT + (prices[i]*quantities[i])
    }


    if (country in europeanCountriesTaxes){

      console.log(country)
      console.log(europeanCountriesTaxes[country])
      taxe=europeanCountriesTaxes[country]
      if(country=="UK" || country=="FR"){
        if(reduction == "PAY THE PRICE"){
          taxe=2
        }
      }else if(country=="BE"){
        res.status(400).send()
        return
      }
      totalTTC=totalHT*taxe

      if (reduction == "PAY THE PRICE"){
        console.log(reduction)
        totalFinal=totalTTC
        res.json({
          total: totalFinal
        })
      }else if (reduction== "HALF PRICE") {
        console.log(reduction)
        totalFinal=totalTTC/2
        res.json({
          total: totalFinal
        })

      }else if (reduction== "STANDARD") {
        for (var i = 0; i<standardReductions.length ; i++) {
          if(i==standardReductions.length){
            if(standardReductions[i].startingPrice<=totalTTC){
              reduction=standardReductions[i].rate
              totalFinal=totalTTC*(1-reduction)
              res.json({
                total: totalFinal
              })
            }

          }else{
            if(standardReductions[i].startingPrice<=totalTTC && standardReductions[i+1].startingPrice >= totalTTC){
              reduction=standardReductions[i].rate
              console.log(totalTTC)
              console.log("reduction")
              console.log(reduction)
              totalFinal=totalTTC*(1-reduction)
              res.json({
                total: totalFinal
              })
              break

            }
          }

        }
      }else {
        res.status(400).send()
      }

    }else {
      res.status(400).send()
    }

  }else {
    res.status(400).send()
  }
};

exports.feedback = function feedback(req, res, next) {
  console.info("FEEDBACK:", req.body.type, req.body.content);
  next();
};

const europeanCountriesTaxes = {
  'DE': 1.2,
  'UK': 1.21,
  'FR': 1.2,
  'IT': 1.25,
  'ES': 1.19,
  'PL': 1.21,
  'RO': 1.2,
  'NL': 1.2,
  'BE': 1.24,
  'EL': 1.2,
  'CZ': 1.19,
  'PT': 1.23,
  'HU': 1.27,
  'SE': 1.23,
  'AT': 1.22,
  'BG': 1.21,
  'DK': 1.21,
  'FI': 1.17,
  'SK': 1.18,
  'IE': 1.21,
  'HR': 1.23,
  'LT': 1.23,
  'SI': 1.24,
  'LV': 1.2,
  'EE': 1.22,
  'CY': 1.21,
  'LU': 1.25,
  'MT': 1.2
};

const standardReductions = [
  { startingPrice: 0, rate: 0 },
  { startingPrice: 1000, rate: 0.03 },
  { startingPrice: 5000, rate: 0.05 },
  { startingPrice: 7000, rate: 0.07 },
  { startingPrice: 10000, rate: 0.1 },
  { startingPrice: 50000, rate: 0.15 },
];
